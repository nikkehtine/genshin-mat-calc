<h1 style="text-align: center;">genshin-mat-calc</h1>

Material calculator for Genshin that lets you calculate all needed materials and store data

## Prerequisites

- **Node** >= 20.2.0
- **pnpm** >= 8.5.1 (might work with regular npm, I haven't tried)
- a web browser

## How to run

1. Clone this repository and open it
2. Run `pnpm install`
3. Run a command:

- `pnpm dev` to run a live preview
- `pnpm build` to build the project
